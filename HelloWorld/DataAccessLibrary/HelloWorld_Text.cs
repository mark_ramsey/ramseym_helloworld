﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace DataAccessLibrary
{
   public class HelloWorld_Text : IHelloWorld
    {
        public HelloWorld_Text()
        {

        }

        public HelloWorld_Text(string FilePathOverLoad)
        {
            _filePath = FilePathOverLoad;
        }


        private string _filePath;

        public string FilePath
        {
            get { return GetMessage(); }
           // set { _filePath = value; }
        }


        public string GetMessage()
        {
            string TextPath = "";
            //Can Change the FilePath setting in the App.Config file to wherever it needs to be.
            try
            {
               TextPath  = ConfigurationSettings.AppSettings["FilePath"].ToString();
            }
            catch
            {
                TextPath = _filePath;
            }
            string RetVal = null;
            StreamReader Reader = new StreamReader(TextPath);
            try
            {
                RetVal = Reader.ReadToEnd();
                Reader.Close();
            }
            catch
            {
                Reader.Close();   
            }
            return RetVal;
        }
        
        public string ReturnMessage()
        {
            return GetMessage();
        }

    }
}
