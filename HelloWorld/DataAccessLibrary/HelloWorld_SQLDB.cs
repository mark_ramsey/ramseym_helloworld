﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLibrary
{
    class HelloWorld_SQLDB : IHelloWorld
    {
        /// <summary>
        /// This class is not implemented but is here to demonstate that by inheriting from the interface IHelloWorld the console output can 
        /// come from any type of datasource we wish to utilize 
        /// </summary>
        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = "Hello World"; }
        }



        public string ReturnMessage()
        {
            return Message;
        }
    }
}
