﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLibrary;

namespace HelloWorldAPI.Controllers
{
    public class HomeController : Controller 
    {
        public ActionResult Index()
        {
            //To make this a more simplified application I placed the text file at the root of the C drive. 
            HelloWorld_Text TextDataSouce = new HelloWorld_Text(@"/HelloWorld_Text.txt");
            string Message = TextDataSouce.ReturnMessage();
            ViewBag.Title = "Home Page";
            ViewBag.Message = Message;
            return View();
        }
    }
}
