﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccessLibrary;
using System.Configuration;

namespace HelloWorldUnitTests
{
    [TestClass]
    public class HelloWorld_TextTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Arrange
            string Message = "Hello World!";
            string RetVal = "";
            //HelloWorld_Text.txt must be in the HelloWorldUnitTests bin\debug directory
            HelloWorld_Text TextDataSouce = new HelloWorld_Text(Environment.CurrentDirectory + @"\HelloWorld_Text.txt");

            //Act
            RetVal = TextDataSouce.ReturnMessage();
            //Assert
            Assert.AreEqual(Message, RetVal);
        }
    }
}
